FROM openjdk:8u212-jre-alpine3.9
RUN mkdir -p /opt/app
RUN chmod -R 777 /opt/
WORKDIR /opt/app
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /opt/app/app.jar" ]
EXPOSE 8080
COPY build/libs/demo2-0.0.1-SNAPSHOT.jar /opt/app/app.jar
RUN touch /opt/app/app.jar