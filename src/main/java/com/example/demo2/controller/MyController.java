package com.example.demo2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @GetMapping
    public String greet(){
        return "Hello World";
    }

    @GetMapping("/pause")
    public String pause() throws InterruptedException {
        Thread.sleep(10000);
        return "Pause";
    }

    @GetMapping("/kill")
    public void kill(){
        System.exit(0);
    }

    @GetMapping("/exception")
    public void myexception(){
        throw new NullPointerException("Dummy");
    }
}
